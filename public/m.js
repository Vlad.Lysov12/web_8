function saveLocalStorage() {
    Stor.setItem("a", $("#a").val());
    Stor.setItem("b", $("#b").val());
    Stor.setItem("c", $("#c").val());
    Stor.setItem("d", $("#d").prop("checked"));
}

function loadLocalStorage() {
    if (localStorage.getItem("a") !== null) {
        $("#a").val(localStorage.getItem("a"));
    }
    if (localStorage.getItem("b") !== null) {
        $("#b").val(localStorage.getItem("b"));
    }
    if (localStorage.getItem("c") !== null) {
        $("#c").val(localStorage.getItem("c"));
    }
    if (localStorage.getItem("d") !== null) {
        $("#d").prop("checked", localStorage.getItem("d") === "true");
        if ($("#d").prop("checked")) {
            $("#sendButton").removeAttr("disabled");
        }
    }
}
function clear() {
    localStorage.clear();
    $("#a").val("");
    $("#b").val("");
    $("#c").val("");
    $("#d").val(false);
}

$(document).ready(function () {
    loadLocalStorage();
    $("#openButton").click(function () {
        $(".fixed-overlay").css("display", "flex");
        history.pushState(true, "", "./form");
    });
    $("#closeButton").click(function () {
        $(".fixed-overlay").css("display", "none");
        history.pushState(false, "", ".");
    });
    $("#form").submit(function (e) {
        e.preventDefault();
        $(".fixed-overlay").css("display", "none");
        // mhtkpoezczaccbtsnz@etochq.com
        // qwerty
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "https://formcarry.com/s/c4XS3E2Irt",
            data: $(this).serialize(),
            success: function (response) {
                if (response.status === "success") {
                    alert("Ваше сообщение отправлено");
                    clear();
                } else {
                    alert("Error`: " + response.c);
                }
            }
        });
    });
    $("#d").change(function () {
        if (this.checked) {
            $("#sendButton").removeAttr("disabled");
        } else {
            $("#sendButton").attr("disabled", "");
        }
    });
    $("#form").change(saveLocalStorage);

    window.onpopstate = function (event) {
        if (event.state) {
            $(".fixed-overlay").css("display", "flex");
        } else {
            $(".fixed-overlay").css("display", "none");
        }
    };
});

